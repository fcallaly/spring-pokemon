package com.allstate.training.springdemo.demo.repo;

import com.allstate.training.springdemo.demo.model.Pokemon;

import java.util.List;

public interface PokemonRepo {

    List<Pokemon> findAll();
}
