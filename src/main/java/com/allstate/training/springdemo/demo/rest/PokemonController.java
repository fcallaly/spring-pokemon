package com.allstate.training.springdemo.demo.rest;

import com.allstate.training.springdemo.demo.model.Pokemon;
import com.allstate.training.springdemo.demo.service.PokemonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PokemonController {

    @Autowired
    private PokemonServiceImpl pokemonServiceImpl;

    @GetMapping
    public List<Pokemon> findAll() {
        return pokemonServiceImpl.findAll();
    }
}
