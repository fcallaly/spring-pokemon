package com.allstate.training.springdemo.demo.repo;

import com.allstate.training.springdemo.demo.model.Pokemon;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PokemonListRepo implements PokemonRepo {

    private List<Pokemon> pokemonTeam = new ArrayList<>();

    public PokemonListRepo() {
        Pokemon pikachu = new Pokemon();
        pikachu.setDexNumber(25);
        pikachu.setName("Pikachu");

        pokemonTeam.add(pikachu);
    }

    public List<Pokemon> findAll() {
        return pokemonTeam;
    }
}
