package com.allstate.training.springdemo.demo.model;

public class Pokemon {

    private int dexNumber;
    private String name;

    public Pokemon() { }

    // chaining constructors!
    public Pokemon(String name) {
        this(-1, name);
    }

    public Pokemon(int dexNumber, String name) {
        this.dexNumber = dexNumber;
        this.name = name;
    }

    public int getDexNumber() {
        return dexNumber;
    }

    public void setDexNumber(int dexNumber) {
        this.dexNumber = dexNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
