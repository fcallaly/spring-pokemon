package com.allstate.training.springdemo.demo.service;

import com.allstate.training.springdemo.demo.model.Pokemon;
import com.allstate.training.springdemo.demo.repo.PokemonRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PokemonServiceImpl {

    private int usefulInt;

    @Autowired
    private PokemonRepo pokemonRepo;

    public List<Pokemon> findAll() {
        return pokemonRepo.findAll();
    }
}
