package com.allstate.training.springdemo.demo.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PokemonTest {

    @Test
    public void pokemonDefaultConstructorTest() {
        String testName = "Piplup";
        int testDexNumber = 393;

        Pokemon testPokemon = new Pokemon();
        testPokemon.setName(testName);
        testPokemon.setDexNumber(testDexNumber);

        assertEquals(testName, testPokemon.getName());
        assertEquals(testDexNumber, testPokemon.getDexNumber());
    }

    @Test
    public void pokemonFullConstructorTest(){
        int testDexNumber = 393;
        String testName = "Piplup";

        Pokemon testPokemon = new Pokemon(testDexNumber, testName);

        assertEquals(testName, testPokemon.getName());
        assertEquals(testDexNumber, testPokemon.getDexNumber());
    }

}
