package com.allstate.training.springdemo.demo.service;

import com.allstate.training.springdemo.demo.model.Pokemon;
import com.allstate.training.springdemo.demo.repo.PokemonRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class PokemonServiceImplTest {

    @Autowired
    private PokemonServiceImpl pokemonServiceImpl;

    @MockBean
    private PokemonRepo pokemonRepo;

    @Test
    public void serviceImplTest() {
        List<Pokemon> testPokemonTeam = new ArrayList<Pokemon>();
        testPokemonTeam.add(new Pokemon(448, "Lucario"));

        when(pokemonRepo.findAll()).thenReturn(testPokemonTeam);

        assertEquals(1, pokemonServiceImpl.findAll().size());
    }
}
